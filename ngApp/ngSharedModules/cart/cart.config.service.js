angular
  .module('LM_ngApp.sharedModules.Cart')
  .service('ConfigCart', ['$log', '$location', '$http', function($log, $location, $http) {
    
    var httpConfig,
        baseURL;
    
    
    /* *********************** GENERAL CONFIG *********************** */
    
    // baseURL e.g. 'https://louismartin.com'
    baseURL = $location.protocol() + '://' + $location.host();
    

    
    
    /* ********************* HTTP CONFIG OBJECT ********************* */
    // default config object passed to $http requests for the cart API
    httpConfig = {
      
      headers: {
        'X-Requested-With': 'XMLHttpRequest'
      }
      
    };
    
    
      
      
      
    /* ************************* UTILITIES ************************** */
    /* joins the default transforms with the current ones and returns them */
    /* https://docs.angularjs.org/api/ng/service/$http#overriding-the-default-transformations-per-request*/
    function appendTransform(defaults, transform) {
      var defaults = angular.isArray(defaults) ? defaults : [defaults];
      return defaults.concat(transform);
    };
      
    
    
    // exports
    this.baseURL = baseURL,
    this.httpConfig = httpConfig;
    
    
  }]);