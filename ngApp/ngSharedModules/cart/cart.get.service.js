angular
  .module('LM_ngApp.sharedModules.Cart')
  .service('CartGet', ['$http', '$log', '$q', 'ConfigCart', function($http, $log, $q, ConfigCart) {
    
    var baseURL = ConfigCart.baseURL + '/cart.js';
    
    
    function getCart() {
      
      // return promise
      return $q(function(resolve, reject) {
        
        $http.get(baseURL).then(function success(data) {
          // successful request: call resolve()
          resolve(data);
          
        }, function error(err) {
          // error response: call reject()
          reject(err);
          
        });
        
      });
    }
    
    
    
    
    
    
    
    
    this.getCart = getCart;
    
    
  }]);