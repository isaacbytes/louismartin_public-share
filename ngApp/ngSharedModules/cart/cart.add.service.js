angular
  .module('LM_ngApp.sharedModules.Cart')
  .service('CartAdd', ['$http', '$log', '$q', 'ConfigCart', function($http, $log, $q, ConfigCart) {
    
    
    // URL where requests are made to 
    var baseURL = ConfigCart.baseURL + '/cart/add.js',
        httpConfig = ConfigCart.httpConfig;
    
    

    
    // Adds product to cart (by id) defaults to quantity 1
    function addToCart(id, quantity, liProps) {
      var liProps = liProps ? liProps : null,
          quantity = quantity ? quantity : 1;

      var data = {
        id: id,
        quantity: quantity,
        properties: liProps
      };


      // return promise
      return $q(function(resolve, reject) {  
        $http.post(baseURL, data, httpConfig).then(function success(data) {
          // successful request: call resolve()
          resolve(data);

        }, function error(err) {
          // error response: call reject()
          reject(err);

        });
      });
    }
    
    

    
    
    
    
    this.addToCart = addToCart;
    
    
  }]);