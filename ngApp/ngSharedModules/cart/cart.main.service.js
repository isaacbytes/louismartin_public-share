angular
  .module('LM_ngApp.sharedModules.Cart')
  .service('CartMain', ['$http', '$rootScope', '$log', '$q', 'CartAdd', 'CartGet', function($http, $rootScope, $log, $q, CartAdd, CartGet) {
    
    /* ________________________
    
    This is the main Service that will host all shared Cart 
    activities. It will handle adding, removing, and getting
    Cart and contents. All other modules wishing to interact
    with the Cart will do so through this Service.
    ________________________ */
    
    var service = this;
    
    
    var cartObj = null;
    var cartQty = null;
    var cartItems = null;
    
    var loadingCart = false;
    var cartLoaded = false;
    
    
    // EXPORTS
    service.add = null;
    service.remove = null;
    service.getCartQty = null;
        
    service.cartQty = null;
    
    
    
    /* INITIALIZE */
    getCart();
    
    
    
    
    
    
    
    /* EXPORT METHODS */
    // ADD TO CART
    service.addToCart = function(id, quantity, properties) {
      // return promise
      return $q(function(resolve, reject) {
        
        CartAdd.addToCart(id, quantity, properties)
          .then(function success(res) {
          
            // cart changed, get new cart qty
            getCart();
          
            // call user callback with res data
            resolve(res);
          
            // broadcast event
            $rootScope.$broadcast('cart:add', res);
          }, function error(err) {
            reject(err);
        });   
      }); 
    };
    
      
    
    
    /* UTILITY FUNCTIONS */
    // GET cart (return promise)
    function getCart() {
      // update state
      loadingCart = true;
      
      // make HTTP request
      CartGet.getCart().then(function success(res){
        // update var
        service.cartQty = res.data.item_count;
        
        // broadcast event
        $rootScope.$broadcast('cart:qty', { qty: res.data.item_count });
        
        // return variable state
        loadingCart = false;
        
      }, function error(res) {
        // Error getting cart
        $log.error(res);
        
        // return variable state
        loadingCart = false;
      });  
    }

    
  }]);