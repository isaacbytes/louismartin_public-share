angular
  .module('LM_ngApp.sharedModules.Products')
  .service('GetProducts', ['$http', '$log', '$q', 'Utils.Http', function($http, $log, $q, utils) {
    
    var config, requestScaffold, sortByUiMapping;
    
    
    // **************************** CONFIGURATION **************************** //
    /* - For information on enums for sorting see the following Shopify Docs link:
    ---- https://help.shopify.com/api/storefront-api/reference/enum/productcollectionsortkeys
    ---- Note that the results come back in ascending order. 
    - */
    // Global AJAX graphQL request configuration:
    config = {
      storeFrontEndpoint            : 'https://louismartin.myshopify.com/api/graphql',
      storeFrontToken               : '3d0ebd75f8dd51c6246cdfdc8c813f7f',
      maxRequestItems               : 250,
      
      defaultSorting                : 'COLLECTION_DEFAULT',
      
      sortByEnums                   : ['BEST_SELLING', 'COLLECTION_DEFAULT',
                                      'CREATED', 'ID', 'MANUAL', 'PRICE',
                                      'RELEVANCE', 'TITLE'],
      
      sortByUiEnums                 : ['COLLECTION_DEFAULT', 'BEST_SELLING',
                                      'PRICE_DES', 'PRICE_ASC', 'CREATED_NEWF',
                                      'CREATED_OLDF'],
      
      // Maps the UI sort keys to the Shopify's RAW request parameters
      sortByUiMapToRaw              : {
                                        'COLLECTION_DEFAULT' : {
                                          sortKey: 'COLLECTION_DEFAULT',
                                          reverse: false
                                        },

                                        'BEST_SELLING' : {
                                          sortKey: 'BEST_SELLING',
                                          reverse: false
                                        },

                                        'PRICE_DES' : {
                                          sortKey: 'PRICE',
                                          reverse: true
                                        },

                                        'PRICE_ASC' : {
                                          sortKey: 'PRICE',
                                          reverse: false
                                        },

                                        'CREATED_NEWF': {
                                          sortKey: 'CREATED',
                                          reverse: false
                                        },

                                        'CREATED_OLDF' : {
                                          sortKey: 'CREATED',
                                          reverse: true
                                        }
                                      }
    };
    
    

    // scaffold structure of Angular HTTP request object
    requestScaffold = {
      method: 'POST',
      url: config.storeFrontEndpoint,
      headers: {
        'X-Shopify-Storefront-Access-Token': config.storeFrontToken
      },
      data: {
        // graphQL query
        query: '',
        
        // graphQL variables
        variables: {}
      },
      transformResponse: utils.appendTransformToDefaults($http.defaults.transformResponse, [function(v) {        
        return utils.transformProductResponse(v);
      }])
    };
    
    
    
    // **************************** HELPER FUNCTIONS **************************** //
    // generate the 'query' string for shopify GraphQL products request
    function getQueryString(isFirstQuery, sortKey, reverse) {
      var sortKey, query;
      
      // Set defaults
      if (typeof isFirstQuery === 'boolean') {
        var isFirstQuery = isFirstQuery;
      } 
      else {
        var isFirstQuery = true;  
      }
      
      if (typeof reverse === 'boolean') {
        var reverse = reverse;
      } 
      else {
        var reverse = false;  
      }
      
      // set sort key 
      sortKey = sortKey || config.defaultSorting;
      
      // build graphql query string (will correspond to 'query' variable in request)
      // Build query string
      query =   'query loadProducts ($handle: String!, $nOfP: Int'
      
        + (isFirstQuery ? ''            :     ', $cursor: String') 
        
        + ') { shop { collectionByHandle (handle: $handle) {... on Collection { products ( sortKey: ' 
        
        + sortKey
      
        + (isFirstQuery ? ', '          :     ', after:$cursor, ')
      
        + (reverse ? 'reverse: true, '  :     'reverse: false, ')
      
        + 'first:$nOfP) {edges {node {... on Product {id, description, createdAt, onlineStoreUrl, productType, publishedAt, tags, title, images(maxHeight: 2400, maxWidth: 2400, first:5) {edges{node{... on Image{src,altText}}}}, variants(first:20) {edges{node{... on ProductVariant{price, availableForSale, compareAtPrice, image(maxHeight: 480, maxWidth: 480){src}}}}}}}, cursor}, pageInfo{hasNextPage}}}}}}';
      
      
      // function returns query string
      return query;
    }
    
    
    
    
    // **************************** RETURN **************************** //
    function getProducts(options) {
      // Required params
      if (!options.colHandle) {
        return $log.error('You must provide a collection handle');
      }
      
      // Config defaults
      var options = {
        colHandle: options.colHandle,
        numProducts: options.numProducts || 15,
        sortBy: options.sortBy || config.defaultSorting,
        cursor: options.cursor || null,
        reverse: false
      };
      
      /* --------------- SETUP --------------- */
      // Validate parameters
      // :numProducts
      if (options.numProducts > config.maxRequestItems) {
        return $log.error('You have requested too many products. Max is ' + config.maxRequestItems);
      }
      // :sortBy
      if (_.indexOf(config.sortByUiEnums, options.sortBy) < 0) {
        return $log.error('Invalid sortBy string. Please see allowed enum values.');
      }
      
      // :reverse - look up correct reverse key in SortBy UI to Raw Mapping above
      options.reverse = config.sortByUiMapToRaw[options.sortBy].reverse;
      
      // set UI sortKey to Raw Shopify GraphQL sortKey
      options.sortBy = config.sortByUiMapToRaw[options.sortBy].sortKey;
      
      /* --------------- BUILD REQUEST --------------- */
      // create copy of request scaffold
      var request                         = angular.copy(requestScaffold);

      // set request variables
      request.data.variables.handle       = options.colHandle;
      request.data.variables.nOfP         = options.numProducts;
      

      // Begin business logic
      // if cursor was provided, fetch items from cursor (not first page)
      if (options.cursor) {
        request.data.variables.cursor     = options.cursor;
        request.data.query                = getQueryString(false, options.sortBy, options.reverse);
      }
      
      else {
        request.data.query                = getQueryString(true, options.sortBy, options.reverse);  
      }
      

      // Make the request! Return promise
      return $q(function(resolve, reject) {        
        $http(request).then(function(data) {
          resolve(data);
        }, 
                            
        function(err) {
          reject(err);
        });
        
      });
    }
    
    
    // add to Service
    this.getProducts = getProducts;
}]);