angular
  .module('LM_ngApp.sharedModules.Products')
  .service('Utils.Http', ['$log', function($log) {
    
    
    
    
    // ************************* HTTP TRANSFORMS ************************* //
    
    /* -*-*- This service holds low-level HTTP transforms applied to the 
       -*-*- incoming Shopify GraphQL response from a products query. A
       -*-*- lot of unnecessary branching makes it hard to traverse in JS.
       -*-*- These methods flatten out the response and provide a consistent
       -*-*- and normalized response API.
    */
    
    

    
    
    
    // ************************* Master functions ************************* //
    
    // (data) => {products:Array, lastProductCursor:String, hasNextPage:Boolean}
    function responseTransform(data) {
      var shopData, responseScaffold, transformedResponse;
      
      // extract the path to the actual data in graphql response
      shopData = data.data.shop.collectionByHandle;
      
      // scaffold of the response 
      responseScaffold = {
        products: [],
        lastProductCursor: '',
        hasNextPage: shopData.products.pageInfo.hasNextPage
      };
      
      // make a copy of the scaffold
      transformedResponse = angular.copy(responseScaffold);
      
      // populate products (delegate fn)
      transformedResponse.products = transformProductEdge(angular.copy(shopData.products.edges));
      
      // populate cursor
      if (shopData.products.edges.length >= 1) {
        transformedResponse.lastProductCursor = shopData.products.edges[shopData.products.edges.length - 1].cursor;  
      }
      
      return transformedResponse;
    }
    
    
    
    
    
    
    
    // ************************* Helper functions ************************* //
    
    // takes in edges array, and returns copy with flattened out properties (images and variants)
    function transformProductEdge(edgesArray) {
      var out = [];
      
      if (!angular.isArray(edgesArray)) {
        return $log.error('Invalid edgesArray input. Input must be an Array');
      }
      
      // populate return array
      for (var i = 0; i < edgesArray.length; i++) {
        // trim node and push into array (make copy to avoid mutating original array)
        out.push(angular.copy(edgesArray[i].node));
        
        // clear bloated properties
        out[i].images = [];
        out[i].variants = [];
        
        // trim images array
        for (j = 0; j < edgesArray[i].node.images.edges.length; j++) {
          out[i].images.push(edgesArray[i].node.images.edges[j].node);
        }
        
        // trim variants array
        out[i].variants = transformVariantsEdge(edgesArray[i].node.variants.edges);
        
        
        /* ********** ADD TO PRODUCT PROPERTIES ********** */
        
        // defaults (later modified in 'getDefaultSaleVariant' fn)
        out[i].LM_variantOnSale = false;
        out[i].LM_hasVariantStock = false;
        
        
        // 'LM_defaultVariant' (first available variant, or default variant in case that none are in stock)
        out[i].LM_defaultVariant = getDefaultSaleVariant(out[i]);
        
        // 'LM_catalogItemFlags' (array of tags that have the '$catFlags$' prefix)
        out[i].LM_catalogItemFlags = getTagsWithPrefix(out[i], '$catFlags$');
        
        // check if product has hidden price
        out[i].LM_isPriceHidden = hasTag(out[i].tags, '$p$odit');
      }
      return out;
    }
    
    
    
    
    // checks product tags and returns an array of tags that begin with a given prefix
    function getTagsWithPrefix(product, prefix) {
      var i, curTag, 
          prefix      = prefix || '', 
          returnArray = [];
      
      
      // if no prefix is provided, return all tags
      if (prefix === '') {
        return angular.copy(product.tags, returnArray);
      }
      
      // loop over tags
      for (var i; i < product.tags.length; i++) {
        curTag = product.tags[i];
        
        // if prefix occurs at position 0
        if (curTag.indexOf(prefix) === 0) {
          // add tag to return array
          returnArray.push(curTag);
        }
      }
      
      return returnArray;
    }
    
    
    
    
    
    
    // 'getDefaultVariant': returns the first available variant that's on sale, or the first not-in-stock
    function getDefaultSaleVariant(product) {      
      var i, curVariant, returnCandidate;
      
      // defaults
      product.LM_variantOnSale = false;
      product.LM_hasVariantStock = false;
      
      
      // search through product variants
      for (i = 0; i < product.variants.length; i++) {
          
        curVariant = product.variants[i];
        
        // check if we have a 'returnCandidate'
        if (returnCandidate) {
          
          // we already have a variant to return but with no sale,
          // so only replace if we find an in-stock AND on sale variant
          if (curVariant.availableForSale === true && isVariantOnSale(curVariant, curVariant.compareAtPrice)) {
            
            product.LM_variantOnSale = true;
            
            // replace 'returnCandidate'
            return angular.copy(curVariant, returnCandidate);
          
          }
        }
        
        
        else {
          // No return candidate, looking for in-stock items...
          if (curVariant.availableForSale === true) {
            // in-stock item found, save as 'returnCandidate'
            angular.copy(curVariant, returnCandidate);
            
            returnCandidate = _.cloneDeep(curVariant);
            
            // update our external Product object - metainfo
            product.LM_hasVariantStock = true;
          }
          
          // if this variant is ALSO on sale, exit and return it
          if (isVariantOnSale(curVariant, curVariant.compareAtPrice)) {
            
            // update our external Product object - metainfo
            product.LM_variantOnSale = true;
            
            // exit and return as default
            return angular.copy(curVariant, returnCandidate);
          }
          
          // if not, keep going to check other variants for a sale 
        } 
      }
      
      
      // if no 'returnCandidate', no in-stock variants were found
      if (!returnCandidate) {
        // update external Product object
        product.LM_hasVariantStock = false;
        
        // just return first variant
        
        // returnCandidate must be of same type as product.variants[0] for copy fn to work
        returnCandidate = {};
        
        angular.copy(product.variants[0], returnCandidate);
      }
      
      // return first variant
      return returnCandidate;
    }
    
    
    
    // takes in variant and a price, returns true if variant is on sale
    function isVariantOnSale(variant, comparePrice) {
      
      var price, comparePrice;
      
      // if no comparePrice, ignore and return false
      if (!comparePrice) {
        return false;
      }
      
      // String - Number coercion [Shopify returns strings for prices]
      price = parseInt(variant.price, 10);
      comparePrice = parseInt(comparePrice, 10);
      
      // Catch NaN from Number coercion. Return false if any are NaN
      if (Number.isNaN(price) || Number.isNaN(comparePrice)) {
        return false;
      }
      
      // Safe to compare prices
      if (price < comparePrice) {
        return true;
      }
      
      return false;
    }
    
    
    
    // takes in image edges array, returns array with flattened out properties (all)
    function transformImageEdge(imgEdges) {
      var out = [];
      
      if (!angular.isArray(imgEdges)) {
        return $log.error('Invalid variantsEdges input. Input must be an Array');
      }
      
      for (i = 0; i < imgEdges.length; i++) {
        out.push(imgEdges[i].node);
      }
      
      return out;
    }
    
    
    
    // takes in variantsEdges array and flattens out their properties (all)
    function transformVariantsEdge(variantsEdges) {
      var out = [];
      
      if (!angular.isArray(variantsEdges)) {
        return $log.error('Invalid variantsEdges input. Input must be an Array');
      }
      
      for (i = 0; i < variantsEdges.length; i++) {
        out.push(variantsEdges[i].node);
      }
      
      return out;
    }
    
    
    
    
    
    // **************************** UTILITIES **************************** //
    
    /* GraphQL 'query' string generator for product requests */
    function getQueryString(isFirstQuery) {
      // Parameter defaults
      var isFirstQuery = isFirstQuery || false;
      
      // Build query string
      var query =        'query loadProducts ($handle: String!, $nOfP: Int'
      
        + (isFirstQuery ? ', $cursor: String'     :     '') 
        
        + ') { shop { collectionByHandle (handle: $handle) {... on Collection { products ('
      
        + (isFirstQuery ? 'after:$cursor,'        :     '') 
      
        + 'first:$nOfP) {edges {node {... on Product {id,description,createdAt,onlineStoreUrl,productType,publishedAt,tags,title,images(maxHeight: 2400, maxWidth: 2400, first:5) {edges{node{... on Image{src,altText}}}},variants(first:1) {edges{node{... on ProductVariant{price,availableForSale}}}}}},cursor},pageInfo{hasNextPage}}}}}}';
      return query;
    }
    
    
    /* joins the default transforms with the current ones and returns them */
    /* --- see https://docs.angularjs.org/api/ng/service/$http#overriding-the-default-transformations-per-request --- */
    function appendTransform(defaults, transform) {

      // We can't guarantee that the default transformation is an array
      var defaults = angular.isArray(defaults) ? defaults : [defaults];

      // Append the new transformation to the defaults
      return defaults.concat(transform);
    };
    
    
    /* filter (boolean) function that rejects products that are unavailable (via onlineStoreUrl property) */
    function isAvailableOnline(product) {
      if (product.onlineStoreUrl) {
        return true;
      }
      return false;
    }
    
    
    // searches product tags for specified tag, returns boolean
    function hasTag(haystack, needle) {
      // search for needle in haystack
      if (_.indexOf(haystack, needle) > -1) {
        return true;
      }
      return false;
    }
    
    
    // returns the allowed sort parameters of GRAPHQL Shopify request
    var allowedRequestSortStrings = ['BEST_SELLING', 'COLLECTION_DEFAULT',
                                      'CREATED', 'ID', 'MANUAL', 'PRICE',
                                      'RELEVANCE', 'TITLE'];
    
    var defaultNormalizedSortStrings = ['COLLECTION_DEFAULT', 'BEST_SELLING',
                                        'PRICE_DES', 'PRICE_ASC', 'CREATED_NEWF',
                                        'CREATED_OLDF'];
    
    
    
    // **************************** FILTERS **************************** //
    function onlyAvailableOnline(productsArray) {
      return _.filter(productsArray, isAvailableOnline(p)); 
    }
    
    

    this.transformProductResponse   = responseTransform;
    this.transformProductEdgeArray  = transformProductEdge;
    this.transformImageEdgeArray    = transformImageEdge;
    this.transformVariantsEdgeArray = transformVariantsEdge;
    this.appendTransformToDefaults  = appendTransform;
    this.productIsAvailableOnline   = isAvailableOnline;
    
    this.allowedRequestSortStrings = allowedRequestSortStrings;
    this.defaultNormalizedSortStrings = defaultNormalizedSortStrings;

  }]);