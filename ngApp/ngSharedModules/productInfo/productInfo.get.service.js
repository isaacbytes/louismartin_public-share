angular
  .module('LM_ngApp.sharedModules.ProductInfo')
  .service('GetProductInfo', ['$http', '$log', '$q', '$location', function($http, $log, $q, $location) {
    
    var getProductByHandle,
        config,
        transformResponse,
        absUrl = $location.protocol() + '://' + $location.host();
    
    
    // Config (raw unmodified response from Ajax API)
    config = {
      
      method: 'GET',
      
      // /products/{product-handle}.js
      url: absUrl + '/products/',
      
      urlSuffix: '.js'
      
    };
    
    
    
    
    // function that handles making the request
    function getProductByHandle(handle) {
      // check for required params
      if (!handle) {
        $log.error('ProductInfo module... Must provide a product handle');
        return;
      }
      
      // return promise
      return $q(function(resolve, reject) {
        var url = config.url + handle + config.urlSuffix;
        
        $http.get(url).then(function(successData) {
          resolve(successData);
           
        }, function(error) {
          reject(error);
        });
        
      });
    }
    
  
  
    function getCurrentProductHandle() {
      if (window.injectedGlobs && window.injectedGlobs.product) {
        if (window.injectedGlobs.product.handle) {          
          return window.injectedGlobs.product.handle;
        }
      }
      
      return $log.error('ProductInfo module... unable to retrieve liquid-injected product handle');
    }
    
    
    
    
    function getDefaultVariant() {
      if (window.injectedGlobs && window.injectedGlobs.selected_or_first_available_variant) {
        return window.injectedGlobs.selected_or_first_available_variant;
      }
      
      return $log.error('ProductInfo module... unable to retrieve liquid-injected product variant');
    }
    
    
    
    function getLiquifiedProductObject() {
      if (window.injectedGlobs && window.injectedGlobs.product) {
        return window.injectedGlobs.product;
      }
      
      $log.error('ProductInfo module... unable to retrieve liquid-injected product handle');
      return false;
    }
    
    
    // return
    this.getProductByHandle = getProductByHandle;
    this.getCurrentProductHandle = getCurrentProductHandle;
    this.getLiquifiedProductObject = getLiquifiedProductObject;
    this.getDefaultVariant = getDefaultVariant;
    
  }]);