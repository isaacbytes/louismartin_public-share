angular
  .module('LM_ngApp.sharedModules.CatalogState')
  .service('CatalogState', [function() {
    
    // state
    this.filters = [];
    this.sortBy = '';
    
    this.filtersMenuOpen = false;
    this.sortByMenuOpen = false;
    
    // methods
    this.clearFilters = function(){};
    
    this.toggleFilter = function(){};
    
    
  }]);
