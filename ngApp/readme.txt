Here you will find the source code for the AngularJS application
that powers many of the features on the webstie I mentioned in my 
application.

These, of course are the development files, before minification and other
transforms are applied with build tools. 

Thank you for taking the time to look.