// Provides sensible defaults in case necessary liquid-injected JS vars are missing

/* 
***     ********** Necessary liquid-injected vars **********     
***     ____________________________________________________
***
***   - window.injectedGlobs.insideCollection           [Boolean]
***   - window.injectedGlobs.collection                 [Object]  - JSON encoded liquid object
***   - window.injectedGlobs.collection.catalogFilters  [Array]   - Array of filter objects, see below
*/



angular.module('LM_ngApp.catalogModule').service('Defaults',['Utils.Http', function(UtilsHttp) {
  
  // ***** DEFAULTS ***** //
  
  // - default collection to load products from
  var defaultCollectionHandle = 'estate';
  
  
  // - fallback data to fill in 'Filters' section when none is provided in liquid
  var defaultCatalogFilters = [
    {
      categoryLabel: 'Stones',
      tagPrefix: '$f$stones$',
      matchedTags: []
    },
    {
      categoryLabel: 'Metals & Precious Metals',
      tagPrefix: '$f$metals$',
      matchedTags: []
    }
  ];
  
  
  // - tags returned from liquid (initialize as null)
  var allTags = null;
  
  
  function populateFields(global) {
    // replace global defaults with the right values
    if (global.injectedGlobs && global.injectedGlobs.collection) {

      // look for collection handle
      if (global.injectedGlobs.collection.handle) {
        defaultCollectionHandle = global.injectedGlobs.collection.handle;
      }
      else {
        $log.error('Collection handle not found! Using fallback handle.');
      }

      // look for LM_filters_sections
      if (global.injectedGlobs.collection.LM_filters_sections) {
        defaultCatalogFilters = global.injectedGlobs.collection.LM_filters_sections;
      }
      else {
        $log.error('Catalog filter map not found! Using fallback handle.');
      }
      
      
      // look for LM_allTags (liquid returns all collection tags)
      if (global.injectedGlobs.collection.LM_allTags) {
        allTags = global.injectedGlobs.collection.LM_allTags;
      }
      else {
        $log.error('Liquid LM_allTags population failed.');
      }

    }
    else {
      $log.error('injectedGlobs.collection not found on global object! Catalog JS code may be dysfunctional');
    }  
  }


  // execute with global object
  populateFields(window);
  
  
  
  this.defaultHandle = defaultCollectionHandle;
  this.defaultFilters = defaultCatalogFilters;
  this.allTags = allTags;
  this.defaultNormalizedSortStrings = UtilsHttp.defaultNormalizedSortStrings;
  
}]);