angular.module('LM_ngApp')
  .controller('scheduleAServiceController', ['$log', '$location', function ($log, $location) {
    
    // https://louismartin.com/pages/schedule-a-service/#!/scheduleService?service=[SERVICE_TITLE]&act=[ACTION_VERB]
    
    // assume the service title coming in is valid
    // accept: a service title (noun); a service action (verb)
    var vm = this;
    vm.inputService = $location.search().service;
    vm.inputActionVerb = $location.search().act;
    vm.noService = true;
    vm.error = null;
    
    // Check if ID was passed into params
    if (vm.inputService) {
      vm.noService = false;
    }
    
    
  }]);