angular.module('LM_ngApp')
  .controller('productViewController', ['$log', '$filter', 'GetProductInfo', 'CartMain', function($log, $filter, GetProductInfo, CartMain) {
    
    
    var vm = this;
    
    /* VIEW-MODEL vars */
    // get product
    vm.curItem = angular.copy(GetProductInfo.getLiquifiedProductObject());
    
    // copy all variants
    vm.curAllVariants = angular.copy(vm.curItem.variants);

    // get default variant (the one to feature onload)
    vm.curDefaultVariant = angular.copy(GetProductInfo.getDefaultVariant());
    
    // set the list of 'filtered' variants i.e. variants that match the chosen option values
    vm.filteredVariants = angular.copy(vm.curItem.variants);
    
    // create default 'options' configuration
    vm.optionsSelection = { option1: null, option2: null, option3: null };
    // populate optionsSelection
    for (var i = 0; i < vm.curDefaultVariant.options.length; i++) {
      vm.optionsSelection['option' + (i+1)] = vm.curDefaultVariant.options[i];
    }
    
        
    
    /* VIEW-MODEL State */
    vm.addingToCart = false;
    vm.addToCartSuccess = false;
    
    vm.addToCartError = false;
    vm.addToCartErrorMessage = '';
    
    
    /* VIEW-MODEL FNs */
    vm.toggleVariantOption = toggleVariantOption;
    vm.isVariantOptionDisabled = isVariantOptionDisabled;
    vm.submitAtC = addToCart;
    
    
    /* FUNCTIONS */
    // ngSubmit function that submits form (adds product to cart)
    function addToCart(inProgress) {
      
      // if adding to cart is already in progress, abort
      if (inProgress) {
        return false;
      }
      
      // update state
      vm.addingToCart = true;
      
      var variant = vm.filteredVariants[0];
      
      $log.info('Adding to cart... Variant')
      $log.info(variant);
            
      CartMain.addToCart(variant.id).then(function success(data) {
        
        // SUCCESS RESPONSE
        vm.addingToCart = false;
        vm.addToCartSuccess = true;
        vm.filteredVariants[0].addedToCart = true;
        $log.info(vm.filteredVariants[0]);
        $log.info('CART... SUCCESS! Response:');
        $log.info(data);
        
        
        // ERROR RESPONSE
      }, function error(err) {
        // update state
        vm.addingToCart = false;
        vm.addToCartSuccess = false;
        vm.addToCartError = true;
        vm.addToCartErrorMessage = err.data.description;

        if (err.data.status === 422) {
          vm.addToCartErrorMessage = 422;
        }
        
        $log.info('CART... ERROR! Response:');
        $log.info(err);
      });
      
    }
    
    
    // toggles selected variant option (on/off)
    function toggleVariantOption(arrayPos, optionVal) {
      var objL = 'option' + (arrayPos + 1);
      
      // if option value is null, assign (toggle on)
      if (vm.optionsSelection[objL] === null) {
         vm.optionsSelection[objL] = optionVal;
          return;
      }
      
      // if option is already active, deactivate (toggle off)
      if (vm.optionsSelection[objL] === optionVal) {
        vm.optionsSelection[objL] = null;
        return;
      }
      // if value is not null AND not optionVal, means another is selected. Must click that one to toggle off
    }

    
    
    
    // predicate determines if this variant option should be disabled based on current user selection
    function isVariantOptionDisabled(optionNum, optionVal) {
      var filterObj, predicate,
          predicateFilterObj = {};
      
      // this is the object that will be used to search through filteredVariants array in 'predicate'
      predicateFilterObj['option' + optionNum] = optionVal;  
      
      // remove null values
      filterObj = _.forOwn(angular.copy(vm.optionsSelection), function(val, key, obj) {
        if ((key === 'option1' || key === 'option2' || key === 'option3') && val === null) {
          delete obj[key];  
        }
      });
            
      // grab currently filtered list of variants  
      vm.filteredVariants = $filter('filter')(vm.curAllVariants, filterObj);
      
      // is this option value inside the current 'filtered' list of variants?
      predicate = $filter('filter')(vm.filteredVariants, predicateFilterObj).length > 0;
      
      return !predicate;
    }
    
    
}]);