angular.module('LM_ngApp')
  .controller('cartWidgetController', ['$log', '$document', '$scope', 'CartMain', function ($log, $document, $scope, CartMain) {
    
    
    var vm = this;    
    vm.cartQty = CartMain.cartQty;
    
    
    
    
    
    
    // respond to cart quantity change events
    $scope.$on('cart:qty', function(event, data) {      
      vm.cartQty = CartMain.cartQty;
    })
    
  }]);