angular.module('LM_ngApp')
  .controller('inquirePriceController', ['$log', '$location', 'GetProductInfo', function ($log, $location, GetProductInfo) {
    
    // https://louismartin.com/pages/inquire-price/#!/inquireProduct?productHandle=[PRODUCT_HANDLE]
    var vm = this;
    vm.givenHandle = $location.search().productHandle;
    vm.noHandle = true;
    vm.error = null;
    
    
    // Check if ID was passed into params
    if (vm.givenHandle) {
      vm.noHandle = false;
    }
    
    
    if (!vm.noHandle) {
      // Lookup the item, notify if lookup fails
      GetProductInfo.getProductByHandle(vm.givenHandle).then(function success(res) {


        // SUCCESS
        vm.matchedProduct = res.data;

      }, function error(errRes) {

        // HANDLE ERROR: invalid handle (404), etc...
        // assign status code to scope variable for UI display
        vm.error = errRes.status;
        
      });
    }
    
    
    

    
  }]);