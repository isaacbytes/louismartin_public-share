angular.module('LM_ngApp')
  .controller('scrollTopWidgetController', ['$log', '$window', '$document', function($log, $window, $document) {
    
    var vm = this;
    
    // Fn variables
    var currentViewportHeight = $window.innerHeight,
        widgetVisibilityDistanceMultipler = 1.5,
        activeScrollDistance = currentViewportHeight * widgetVisibilityDistanceMultipler;
    

    // Scope state
    vm.widgetVisible = false;
    vm.test = true;
    
    
    // react to scroll event
    $document.on('scroll', function() {
      
      
      if (!vm.widgetVisible && $document.scrollTop() > activeScrollDistance) {
        // make visible
        vm.widgetVisible = true;
      }
      
      
      if (vm.widgetVisible && $document.scrollTop() < activeScrollDistance) {
        // make visible
        vm.widgetVisible = false;
      }
      
    });
    
    
    
}]);