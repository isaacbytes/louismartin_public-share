angular.module('LM_ngApp')
  .controller('productsRenderCtrl', ['$log', 'Defaults', 'GetProducts', 'CatalogState', function ($log, Defaults, GetProducts, CatalogState) {
  
  
    var collectionHandle, catalogFilters, allTags, vm;

    // DEFAULTS / FALL-BACKS                                   
    catalogFilters    = Defaults.defaultFilters;
    collectionHandle  = Defaults.defaultHandle;
    allTags = Defaults.allTags;

    
    
    vm = this;

    /* --------------- Scope-bound state --------------- */
    // application state                                  
    vm.allProducts = [];
    vm.lastItemCursor = null;
    vm.tags = allTags || [];

    // UI state
    vm.products = [];
    vm.filters = [];
    vm.sortBy = 'COLLECTION_DEFAULT';
    vm.hasNextPage = true;
    vm.loadingProducts = false;
    vm.loadingProductsError = false;
    vm.catalogFilters = [];

    vm.filtersMenuOpen = CatalogState.filtersMenuOpen;
    vm.sortByMenuOpen = false;

    vm.sortOptions = Defaults.defaultNormalizedSortStrings;



    /* --------------- Initialization --------------- */
    // make local copy of catalog filters variable                                  
    angular.copy(catalogFilters, vm.catalogFilters);
    
    // define variables
    var toggleFiltersMenu, toggleSortByMenu, changeSort,
        getMoreProducts, clearFilters, makeTheRequest, updateTags;



    /* --------------- Scope-bound functions --------------- */
    vm.logProduct = function (product) {   $log.info(product); };

    vm.toggleFiltersMenu  = toggleFiltersMenu;
    vm.toggleSortByMenu   = toggleSortByMenu;
    vm.changeSort         = changeSort;
    vm.getMoreProducts    = getMoreProducts;
    vm.clearFilters       = clearFilters;


    /* --------------- Initial request of products --------------- */
    vm.getMoreProducts();


    /* FUNCTIONS */
    // toggle filtersMenu open/close                                   
    function toggleFiltersMenu() {
      if (vm.filtersMenuOpen === true) {
        vm.filtersMenuOpen = false;
        return;
      }
      vm.filtersMenuOpen = true;
    }



    // toggle sortBy menu open/close                                   
    function toggleSortByMenu() {
      if (vm.sortByMenuOpen === true) {
        vm.sortByMenuOpen = false;
        return;
      }
      vm.sortByMenuOpen = true;
    }



    // handle sortBy changes                                   
    function changeSort(sortOption) {
      // return if currently loading products
      if (vm.loadingProducts) {
        return false;
      }


      // Was a new option even selected? If not, exit
      if (sortOption === vm.sortBy) {
        return false;
      }



      // update Sort vm variable
      vm.sortBy = sortOption;

      toggleSortByMenu();

      // make request
      vm.getMoreProducts({clearProducts: true});
    }



    //  UI function that handles making products request
    function getMoreProducts(options) {
      /* ********** CHECK PARAMS ********** */
      var options = options || {},
          requestSize = 50;


      // :clearProducts
      if (options.clearProducts) {
          options.clearProducts = true;
      }
      else {
          options.clearProducts = false;
      }

      if (options.getAll) {
          requestSize = 100;
      }


      /* ********** UPDATE STATE ********** */
      // update state variable
      vm.loadingProducts = true;


      /* ********** NEW REQUEST? ********** */
      if (options.clearProducts) {
        vm.lastItemCursor = null;
        vm.products = [];
        vm.allProducts = [];
//- removed 9/27/18 for new liquid-generated tags that don't update upon request
//-        vm.tags = [];
      }

      /* ********** MAKE THE REQUEST ********** */
      // make the http request (save in a function for recursivity)
      makeTheRequest();


      function makeTheRequest() {
        // ######## function start ######## //
        GetProducts.getProducts({
          colHandle: collectionHandle,
          numProducts: requestSize,
          cursor: vm.lastItemCursor,
          sortBy: vm.sortBy

        // SUCCESS 
        }).then(function(response) {
          // GETTING ALL PRODUCTS?
          if (options.getAll) {

            // Updates...
            vm.products = vm.products.concat(response.data.products);
            updateTags(vm.products);
                        
            vm.allProducts = vm.allProducts.concat(response.data.products);
            vm.lastItemCursor = response.data.lastProductCursor;

            // Make another request?
            if (response.data.hasNextPage) {
              // recursive request
              makeTheRequest();
            } else {
              // Done loading everything! 
              // make pertinent updates

              vm.hasNextPage = response.data.hasNextPage;

              // return variable state
              vm.loadingProducts = false;
            }
          }

          else {
              // update error UI state
              vm.loadingProductsError = false;

              // update products array
              vm.products = vm.products.concat(response.data.products);
              updateTags(vm.products);

              // update master products record
              vm.allProducts = vm.allProducts.concat(response.data.products);

              // get cursor
              vm.lastItemCursor = response.data.lastProductCursor;

              // has next page?
              vm.hasNextPage = response.data.hasNextPage;

              // return variable state
              vm.loadingProducts = false;
          }

        // ERROR
        }, function (error) {
          $log.error('Unhandled rejection! Provide user feedback that loading/request error. Error from server (response)');
          vm.loadingProducts = false;
          vm.loadingProductsError = true;
        });

      } // ######## function end ######### //
    }




    // adds/removes tag from filters array                                   
    vm.toggleFilter = function (tag) {
      var loc = _.indexOf(vm.filters, tag);

      if (loc > -1) {
        // filter already applied, remove
        vm.filters.splice(loc, 1);
      }
      else {
          // filter not present, apply
          vm.filters.push(tag);

          // LOAD all products on filter applying
          if (vm.hasNextPage && !vm.loadingProducts) {
            getMoreProducts({getAll: true});
          }
      }

    };


    // returns true if product passes active filters                                   
    vm.filterPredicate = function(val, index, arr) {
      var i;
      // loop over filters
      for (i = 0; i < vm.filters.length; i++) {
        var curFilter = vm.filters[i];

        // if filter is not in product tags, return false to remove from array
        if (_.indexOf(val.tags, curFilter) <= -1) {
          return false;
        }
      }
      // passed the loop check, keep product
      return true;
    };


    // returns true if tag is an active filter, false if it's not                                   
    vm.hasFilter = function(tag) {
      if (_.indexOf(vm.filters, tag) > -1) {
        return true;
      }
      return false;
    };                                  



    /* --------------- Util - functions --------------- */
    // takes in a productsArray and joins it with vm.tags without duplicates
    function updateTags(productsArray) {
      var i;
      
      // for each product in products array
      for (i = 0; i < productsArray.length; i++) {
        // create union of two arrays (no dupes)
        vm.tags = _.union(productsArray[i].tags, vm.tags);
      }

      // run the catalog-filter tag-matches
      updateCatalogFilterTags();
    }



    // clears filters on view
    function clearFilters() {
      vm.filters = [];
    }



    // runs through vm.tags and updates Catalog Filter tags
    function updateCatalogFilterTags() {
      var i, j, curTag, curCatalogFilter;


      // check if catalogFilters array exists
      if (!(vm.catalogFilters.length && vm.catalogFilters.length > 0)) {
        $log.info('No catalog filter tag patterns provided');
        return false;
      }


      // loop through vm.tags
      for (i = 0; i < vm.tags.length; i++) {
        curTag = vm.tags[i];


        // for each tag
        for (j = 0; j < vm.catalogFilters.length; j++) {
          curCatalogFilter = vm.catalogFilters[j];


          // search through tagMatchPatterns for a match
          if (curTag.indexOf(curCatalogFilter.tagPrefix) === 0) {

            // if tag is not already in 'matchedTags', add
            if (_.indexOf(curCatalogFilter.matchedTags, curTag) === -1) {
              curCatalogFilter.matchedTags.push(curTag);  
            }

          }

          // no match found
        }
      }

    }
  
  
}]);