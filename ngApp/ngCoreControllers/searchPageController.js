angular.module('LM_ngApp')
  .controller('searchPageCtrl', ['$log', '$location', '$window', function($log, $location, $window) {
    
  var vm = this;
    
    
  vm.formData = { q: '' };
  vm.emptyCell = '';
    
  if (window.injectedGlobs.search.searchTerms) {
    vm.formData.q = window.injectedGlobs.search.searchTerms;
  }
   
    
  vm.searchArticles = function() {
    $window.location.href = '/search/?type=page,article&q=' + vm.formData.q;
  };
    
    
  }]);