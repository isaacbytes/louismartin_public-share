// REGISTER APP
angular.module('LM_ngApp', ['ngAnimate',
                            'ngRoute',
                            'duScroll',
                            'LM_ngApp.catalogModule',
                            'LM_ngApp.sharedModules.ProductInfo',
                            'LM_ngApp.sharedModules.Products',
                            'LM_ngApp.sharedModules.Cart',
                            'LM_ngApp.sharedModules.CatalogState',
                            'LM_ngApp.filters']);


// ANGULAR APP CONFIG
angular.module('LM_ngApp').config(['$interpolateProvider', function($interpolateProvider) {
  
  // change interpolation symbols to not conflict with Liquid
  $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
  
  
}]);