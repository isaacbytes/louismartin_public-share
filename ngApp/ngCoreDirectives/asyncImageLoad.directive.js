// directive for async loading of images
// primarily for collections page, it will accept a loading gif and replace 
// it with the img src once it has loaded

angular.module('LM_ngApp')
  .directive('lmAsyncImgSrc', ['$window', function($window) {
    

    return {
      restrict: 'A',
      link: function(scope, element, attrs) {
        
        var trueImg = new Image();
        
        
        // when this image loads, replace the loading gif
        trueImg.onload = function() {
          element[0].src = trueImg.src;
        }
        
        
        // load the image
        trueImg.src = attrs.lmAsyncImgSrc;

      }
      
    };

}]);