// **************************** CONFIGURATION **************************** //
/* - This filter takes in the Shopify Product Ajax API price property and 
---- normalizes the price, which for some reason comes back as 100x the original.
---- It seems they want to preserve the trailing zeroes in theyr full format.
- */


angular
  .module('LM_ngApp.filters')
  .filter('product_shopifyPriceTransform', function() {
  
    
  
  
    return function(input) {
      var out;
      
      // check if input is a number
      if (typeof input !== 'number') {
        // not a number, return unmodified original
        return input;
      }
      
      // check if input already has a '.' delimiter - NEED TO DO
        // already in decimal format, return unmodified original
      
      out = input / 100;
      
      return out;
      
    };
  
  });