// **************************** CONFIGURATION **************************** //
/* - This filter takes in HTTP error status codes (int) from the AJAX Cart 
---- response and returns user-friendly responses.
- */


angular
  .module('LM_ngApp.filters')
  .filter('product_errorMessages', function() {
  
    
  
  
    return function(input) {
      var out = input;
      
      
      // 422 (max stock in cart)
      if (input === 422) {
        out = "Max quantity already in cart."
      }
      
      
      
      return out;
      
    };
  
  });