// **************************** CONFIGURATION **************************** //
/* - This filter takes in true/false and returns appropriate 'availability' string
- */


angular
  .module('LM_ngApp.filters')
  .filter('product_availability', function() {
  
    
  
  
    return function(input) {
      var out = input;
      
      
      // available
      if (input === true) {
        out = "available";
      }
      
      if (input === false) {
        out = "unavailable";
      }
      
      
      
      return out;
      
    };
  
  });