// **************************** CONFIGURATION **************************** //
/* - This filter takes in the internal sort key in the catalogViewController
---- and filters it into user-friendly English text for the SORT widget.
- */


angular
  .module('LM_ngApp.filters')
  .filter('catalog_normalizeSortKey', function() {
  
  
    var internalToFriendly = {
      'COLLECTION_DEFAULT'      :       'Featured',
      'BEST_SELLING'            :       'Best selling',
      'PRICE_DES'               :       'Price (high - low)',
      'PRICE_ASC'               :       'Price (low - high)',
      'CREATED_NEWF'            :       'Newest first',
      'CREATED_OLDF'            :       'Oldest first'
    };
  
    return function(input) {
      
      // check if key exists in library
      if (internalToFriendly[input]) {
        return internalToFriendly[input];
      }
      
      // if not, returned unmodified input
      return input;
    };
  
});