angular
  .module('LM_ngApp.filters')
  .filter('text_utils_capitalize', function() {
  
    // capitalizes first letter of the word / string
    return function(input) {    
      // if input exists and is a string, replace first character with uppercase
      return (angular.isString(input) && input.length > 0) ? input[0].toUpperCase() + input.substr(1).toLowerCase() : input;
    };
  
})
  .filter('separate_underscore', function() {
  
    // escape special characters in string
    function escapeRegExp(string){
      return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
    }
    
  
    // takes in a string and replaces underscores with spaces
    return function(input) {
      var modified, original = input;
      
      // if empty string, just return
      if (!input) {
        return input;
      }
      
      // perform RegEx match-and-replace
      modified = input.replace(new RegExp(escapeRegExp('_'), 'g'), ' ');
      return modified;
    }
});