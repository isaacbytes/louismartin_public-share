// **************************** CONFIGURATION **************************** //
/* - Default catalog filter format: $f$[filter_category]$[value]
---- This is the fallback in case no prefix is provided as an argument
---- into the filter predicate function. Will search for this RegEx 
---- pattern.
- */


angular
  .module('LM_ngApp.filters')
  .filter('catalog_extractTagLabel', function() {
  
  
    // extracts the label portion of the tag, provided the prefix
    return function(input, labelPrefix) {
      var   input       = input || '',
            labelPrefix = labelPrefix || '',
            out         = '',
            matchPat    = /\$\w+\$\w+\$/,
            intermed,
            substrLocation;
      
      
      
      // IF NO PREFIX PROVIDED, USE STD REGEX pattern matching
      if (!labelPrefix) {
        // split string based on RegEx (limit: 1 match)
        intermed = input.split(matchPat);
                
        // extract last elem of array
        out = intermed[intermed.length-1];
      }
      
      
      // IF PREFIX PROVIDED, MATCH PREFIX AND SLICE
      else {
        // look for prefix end index
        substrLocation = input.lastIndexOf(labelPrefix);

        // if not found, return unmodified output
        if (substrLocation === -1) {
          out = input;
          return out;
        }

        // match found, slice the string
        out = input.replace(labelPrefix, '');
      }
      
      // replace any occurrences of '_' with ' '
      out = out.replace(new RegExp('_', 'g'), ' ');
      
      return out;
    };
  
});